package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConnection {
    private Connection connection;

    public DatabaseConnection(String url, String user, String password) throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    public void saveMahasiswa(String nim, String nama, String angkatan, String jurusan, String tgl_lahir, String kota_lahir) throws SQLException {
        String query = "INSERT INTO identitas (nim, nama, jurusan, angkatan, tgl_lahir, kota_lahir) VALUES ('" + nim + "', '" + nama + "', '" + jurusan + "', '" + angkatan + "', '" + tgl_lahir + "', '" + kota_lahir + "')";
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public ResultSet getMahasiswaData() throws SQLException {
        String query = "SELECT * FROM identitas";
        Statement statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    public void close() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }
}
