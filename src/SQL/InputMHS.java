package SQL;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InputMHS extends JFrame {
    private JTextField nimField;
    private JTextField namaField;
    private JTextField jurusanField;
    private JTextField angkatanField;
    private JTextField tgl_lahirField;
    private JTextField kota_lahirField;
    private JTable table;
    private DefaultTableModel tableModel;
    private DatabaseConnection dbConnection;

    public InputMHS() {
        setTitle("Input Data Mahasiswa");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel inputPanel = new JPanel(new GridLayout(7, 4));
        JLabel nimLabel = new JLabel("NIM:");
        nimField = new JTextField();
        JLabel namaLabel = new JLabel("Nama:");
        namaField = new JTextField();
        JLabel jurusanLabel = new JLabel("Jurusan:");
        jurusanField = new JTextField();
        JLabel angkatanLabel = new JLabel("Angkatan:");
        angkatanField = new JTextField();
        JLabel tgl_lahirLabel = new JLabel("Tanggal Lahir:");
        tgl_lahirField = new JTextField();
        JLabel kota_lahirLabel = new JLabel("Kota Lahir:");
        kota_lahirField = new JTextField();

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });

        inputPanel.add(nimLabel);
        inputPanel.add(nimField);
        inputPanel.add(namaLabel);
        inputPanel.add(namaField);
        inputPanel.add(jurusanLabel);
        inputPanel.add(jurusanField);
        inputPanel.add(angkatanLabel);
        inputPanel.add(angkatanField);
        inputPanel.add(tgl_lahirLabel);
        inputPanel.add(tgl_lahirField);
        inputPanel.add(kota_lahirLabel);
        inputPanel.add(kota_lahirField);
        inputPanel.add(new JLabel());
        inputPanel.add(saveButton);

        add(inputPanel, BorderLayout.NORTH);

        tableModel = new DefaultTableModel();
        table = new JTable(tableModel);
        add(new JScrollPane(table), BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        JButton loadButton = new JButton("Load Data");
        buttonPanel.add(loadButton);
        add(buttonPanel, BorderLayout.SOUTH);

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadData();
            }
        });

        try {
            dbConnection = new DatabaseConnection("jdbc:mysql://localhost:3306/mahasiswa", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        String nim = nimField.getText();
        String nama = namaField.getText();
        String jurusan = jurusanField.getText();
        String angkatan = angkatanField.getText();
        String tgl_lahir = tgl_lahirField.getText();
        String kota_lahir = kota_lahirField.getText();

        try {
            dbConnection.saveMahasiswa(nim, nama, angkatan, jurusan, tgl_lahir, kota_lahir);
            JOptionPane.showMessageDialog(this, "Data saved successfully");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error saving data");
        }
    }

    private void loadData() {
        try {
            ResultSet rs = dbConnection.getMahasiswaData();
            tableModel.setRowCount(0); // Clear existing data
            int columnCount = rs.getMetaData().getColumnCount();
            tableModel.setColumnCount(columnCount);
            tableModel.setColumnIdentifiers(new String[]{"NIM", "Nama","Jurusan", "Angkatan", "Tanggal Lahir", "Kota Lahir"});

            while (rs.next()) {
                Object[] row = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    row[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error loading data");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new InputMHS().setVisible(true);
            }
        });
    }
}